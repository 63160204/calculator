import 'dart:io';

class calculator {
  double n1 = 0;
  double n2 = 0;
  double sum = 0;

  cal(double n1, double n2) {
    this.n1 = n1;
    this.n2 = n2;
  }

  double add(double n1, double n2) {
    sum = n1 + n2;
    return sum;
  }

  double del(double n1, double n2) {
    sum = n1 - n2;
    return sum;
  }

  double div(double n1, double n2) {
    sum = n1 / n2;
    return sum;
  }

  double mul(double n1, double n2) {
    sum = n1 * n2;
    return sum;
  }
}

void main() {
  calculator cal = new calculator();

  print("input 1 = Add");
  print("input 2 = Del");
  print("input 3 = Mul");
  print("input 4 = Div");
  print("input 5 = Exit");
  print(" Enter your Number");

  int? menu = int.parse(stdin.readLineSync()!);
  while (menu != 5) {
    if (menu == 1) {
      print("input n1 and n2");
      double? n1 = double.parse(stdin.readLineSync()!);
      double? n2 = double.parse(stdin.readLineSync()!);
      print(cal.add(n1, n2));
    }
    if (menu == 2) {
      print("input n1 and n2");
      double? n1 = double.parse(stdin.readLineSync()!);
      double? n2 = double.parse(stdin.readLineSync()!);
      print(cal.del(n1, n2));
    }
    if (menu == 3) {
      print("input n1 and n2");
      double? n1 = double.parse(stdin.readLineSync()!);
      double? n2 = double.parse(stdin.readLineSync()!);
      print(cal.mul(n1, n2));
    }
    if (menu == 4) {
      print("input n1 and n2");
      double? n1 = double.parse(stdin.readLineSync()!);
      double? n2 = double.parse(stdin.readLineSync()!);
      print(cal.div(n1, n2));
    }
    menu = int.parse(stdin.readLineSync()!);
  }
  if (menu == 5) {
    print("Exit Program");
  }
}
